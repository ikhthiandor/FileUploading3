<?php
/**
 * Created by PhpStorm.
 * User: ikhthiandor
 * Date: 3/11/15
 * Time: 9:38 PM
 */

class UsersController extends BaseController{

    public function register(){

        return Response::view('layouts.register');
    }


    public function postregister(){

        //TODO: save user to database
        $user = new User;
        $user->username = Input::get('username');
        $user->email = Input::get('email');
        $user->password = Hash::make(Input::get('password'));
        $user->save();


        return "<h1> Saved user to database </h1>";

//        return Input::all();
    }

    public function login() {
        return Response::view('layouts.login');
    }


    public function postlogin() {
        return "<h1>You are logged in</h1>";
    }

} 