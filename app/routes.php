<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
	return View::make('hello');
});

Route::get('/home', function(){
   return Response::view('layouts.home');
});

Route::get('/register', 'UsersController@register');
Route::post('/register', 'UsersController@postregister');

Route::get('/login', 'UsersController@login');
Route::post('/login', 'UsersController@postlogin');





