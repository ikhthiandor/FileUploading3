<!doctype html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>Register</title>

  <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css"/>

</head>
<body>

  {{ Form::open() }}

  <div class="form-group">
  {{ Form::label('username', 'Username:') }}
  {{ Form::text('username', '', ['class' => 'form-control']) }}

  </div>


  <div class="form-group">
  {{ Form::label('email', 'Email:') }}
  {{ Form::text('email', '', ['class' => 'form-control']) }}

  </div>

  <div class="form-group">

  {{ Form::label('password', 'Password:') }}
  {{ Form::password('password', ['class' => 'form-control']) }}


  </div>

  <div class="form-group">

  {{ Form::label('password_confirmation', 'Password Confirmation:') }}
  {{ Form::password('password_confirmation', ['class' => 'form-control']) }}

  {{ Form::submit('Register', ['class' => 'btn btn-primary']) }}


  </div>





  {{ Form::close() }}




</body>
</html>

