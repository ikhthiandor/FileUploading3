<!doctype html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>Login page</title>

<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css" rel="stylesheet">

</head>
<body>
 <h1>Sign in!</h1>

{{ Form::open() }}

	<div class="form-group">
	
	{{ Form::label('email', 'Email', ["class" => 'control-label']) }}
    {{ Form::email('email', '', ['class' => 'form-control', 'required' => 'required']) }}

	
	</div>
			
			
	<div class="form-group">
	
	{{ Form::label('password', 'Password:', ['class' => 'control-label', 'required' => 'required']) }}
    {{ Form::password('password', ['class' => 'form-control']) }}

	</div>


	<div class="form-group">

	{{ Form::submit('Login', ['class' => 'btn btn-primary']) }}

	</div>
	
{{ Form::close() }}

</body>
</html>